# IMPORTS

from tqdm import tqdm
import os
import cv2
import math
import numpy as np

# FUNCTIONS

def crop_img(rectangle, frame):
    """Crops a patch in a frame"""

    # frame = np.array(frame, dtype=np.float32)

    rect = ((float(rectangle[1]), float(rectangle[2])), (float(rectangle[3]), float(rectangle[4])), float(rectangle[5]) * (180.0/math.pi))

    box = cv2.boxPoints(rect).astype(np.int0)

    # get width and height of the detected rectangle
    width = int(rect[1][0])
    height = int(rect[1][1])

    src_pts = box.astype("float32")

    dst_pts = np.array([[0, height-1],
                        [0, 0],
                        [width-1, 0],
                        [width-1, height-1]], dtype="float32")

    # the perspective transformation matrix
    M = cv2.getPerspectiveTransform(src_pts, dst_pts)

    # directly warp the rotated rectangle to get the straightened rectangle
    warped = cv2.warpPerspective(frame, M, (width, height))

    return warped

if __name__ == "__main__":

    path_images = 'test_images'
    path_occupied_rec = 'test_images\\labeled_images'
    path_patches = 'frame_4.txt'
    path_occupied_patches = 'patches\\1'
    path_free_patches = 'patches\\0'
    patches = {}
    flag = False

    with open(path_patches, 'r') as p:
        for idx, line in enumerate(p):
            patches[idx] = line.strip().split(' ')

    # print(patches)

    for image in tqdm(os.scandir(path_images)):
        if image.path.endswith(".jpg") and image.is_file():
            print(image.path)
            patch_name = image.name.split('.')
            picture = cv2.imread(image.path)

            # for occupied_rec in os.scandir(path_occupied_rec):
            #     if occupied_rec.path.endswith(".txt") and occupied_rec.name != 'classes.txt' and occupied_rec.is_file():
            #         # print(occupied_rec.path)
            

            for counter, patch in enumerate(patches.values()):
                # print(patch)

                f = open(path_occupied_rec + '\\' + patch_name[0] + '.txt', 'r')

                for line in f:
                    content = line.split(' ')
                    x_min = float(content[1]) - (float(content[3])/2)
                    x_max = x_min + float(content[3])
                    y_min = float(content[2]) - (float(content[4])/2)
                    y_max = y_min + float(content[4])

                    print(f'{patch_name[0]} min, max: {x_min}, {x_max}, {y_min}, {y_max}')

                    print(f'patch: {float(patch[1])}, {float(patch[2])}')

                    if (x_min < float(patch[1]) < x_max) and (y_min < float(patch[2]) < y_max):

                        flag = True
                        break
                  
                    else:

                        flag = False

                    print('\n')
                
                if flag == True:
                    if not os.path.exists(path_occupied_patches):
                        os.mkdir(path_occupied_patches)

                    p = crop_img(patch, picture)
                    cv2.imwrite(path_occupied_patches + f'\\{patch_name[0]}_{counter}.jpg', p)

                    print('11111111111111111111')
                else:
                    if not os.path.exists(path_free_patches):
                        os.mkdir(path_free_patches)

                    p = crop_img(patch, picture)
                    cv2.imwrite(path_free_patches + f'\\{patch_name[0]}_{counter}.jpg', p)

                    print('000000000000000000000')
                
                flag = False








