# IMPORTS

from tqdm import tqdm
import os

# FUNCTIONS

def create_file(directory, file_name):
	""" Creates a file and a header in that file"""

	complete_name = os.path.join(directory, file_name)
	dash = '-' * 90
    
	with open(complete_name, 'a') as f:
	    f.write(dash + '\n')
	    f.write("{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}".format('STATUS', 'CENTER_X', 'CENTER_Y', 'HEIGHT', 'WIDTH', 'ANGLE').strip())
	    f.write('\n' + dash)
	    f.close()

def save_patch(status, directory, file_name, patch):
	""" Saves patch data into a file"""

	complete_name = os.path.join(directory, file_name)
	with open(complete_name, 'a') as f:
	    if status == True:
	        f.write("{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}".format('\noccupied', patch[1], patch[2], patch[3], patch[4], patch[5]))
	        f.close()
	    else:
	        f.write("{:<15}{:<15}{:<15}{:<15}{:<15}{:<15}".format('\nfree', patch[1], patch[2], patch[3], patch[4], patch[5]))
	        f.close()


if __name__ == "__main__":

    # Paths to 
    path_images = 'test_images'
    path_occupied_rec = 'test_images/labeled_images'
    path_patches = 'frame_4.txt'
    path_data = 'patches/'
    patches = {}
    flag = False

    # Fills a dictionary with the drawn patches data
    with open(path_patches, 'r') as p:
        for idx, line in enumerate(p):
            patches[idx] = line.strip().split(' ')

    # For each .txt file with coordinates of labeled reactangles (big ones)
    for file in tqdm(os.scandir(path_occupied_rec)):
        if file.path.endswith(".txt") and file.name != 'classes.txt' and file.is_file():

            # For each entity in the dictionary with patches data
            for counter, patch in enumerate(patches.values()):
                f = open(file.path, 'r')

                # For each line in a .txt file with coordinates of labeled rectangles (big ones) 
                for line in f:
                    content = line.split(' ')
                    x_min = float(content[1]) - (float(content[3])/2)
                    x_max = x_min + float(content[3])
                    y_min = float(content[2]) - (float(content[4])/2)
                    y_max = y_min + float(content[4])

                    # If the center of a patch is inside of a rectangle
                    if (x_min < float(patch[1]) < x_max) and (y_min < float(patch[2]) < y_max):

                        flag = True
                        break
                    
                    else:

                        flag = False

                
                if flag == True:
                    if not os.path.exists(path_data):
                        os.mkdir(path_data)

                    if not os.path.exists(path_data + file.name):
                        create_file(path_data, file.name)

                    save_patch(True, path_data, file.name, patch)

                else:
                    if not os.path.exists(path_data):
                        os.mkdir(path_data)

                    if not os.path.exists(path_data + file.name):
                        create_file(path_data, file.name)

                    save_patch(False, path_data, file.name, patch)
                
                flag = False