# IMPORTS

import os
import cv2
import math
import numpy as np
from tqdm import tqdm

# FUNCTIONS

def load_coordinates(coordinates_path):
    """Returns a list of rectangles coordinates"""

    coordinates = {}
    with open(coordinates_path, 'r') as f:
        for i, line in enumerate(f.readlines()):
            line = line.strip()
            rectangle = line.split(" ")
            coordinates[i] = rectangle 
        return coordinates

def box_making(rectangle):
    """Returns the bounding box of a parking spot"""

    rect = ((float(rectangle[1]), float(rectangle[2])), (float(rectangle[3]), float(rectangle[4])), float(rectangle[5]) * (180.0/math.pi))
    box = cv2.boxPoints(rect).astype(np.int0)
    return box

def save_result(box_list, image, path):
    """"Draws bounding boxes in all images"""

    # If label_images directory does not exist, create
    if not os.path.exists(path):
        os.mkdir(path)
        os.chdir(path)
    
    file_name = image.path.rstrip('\r\n').split('\\')[-1]
    image = cv2.imread(image.path)
    cv2.drawContours(image, box_list, -1, (0,255,0), 2)
    cv2.imwrite(file_name, image)


if __name__ == "__main__":

    # Change the file name to that one which you used for labeling 
    coordinates_path = os.getcwd() + '\\frame_4.txt'
    rectangles_coordinates = load_coordinates(coordinates_path)
    box_list = []

    for rectangle in rectangles_coordinates.values():
        box = box_making(rectangle)
        box_list.append(box)

    # '\\test_images' is the direcory where original (raw) images have to be stored
    os.chdir(os.getcwd() + '\\test_images')
    # '\\labeled_images' is the direcory (inside of '\\test_images') where labled images are stored 
    path = os.getcwd() + '\\labeled_images'

    for image in tqdm(os.scandir(os.getcwd())):
        save_result(box_list, image, path)


    

