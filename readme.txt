1. Choose one of the original images (from the dataset which has to be annotated) and open it in labelImg-kitti.
2. Draw rectangles at parking places (important: when drawing rectangles, create a new rectangle each time, do not copy the previous one. If you copy, the angle of the copied rectangle will be zero. This is a bug of labelImg-kitti).
3. Save the labeled data into the root directory in the kitti format.
4. Launch labelling.py (important: if it is needed, change the file name with data (previous step) and paths in the script (pay attention to forward and backslashes in the paths. For Windows and Linux they are different)).
5. Open the created directory with labeled images in labelImg-kitti.
6. For each image, draw rectangles around the areas where parking places are occupied (means that a few parking places can be in one big rectangle) and save in the same directory where the labeled images are in the kitti format.
7. Launch saving.py (important: if it is needed, modify the script: change the file name with the labeled data from step #2 and the paths where the resulting data will be saved).
